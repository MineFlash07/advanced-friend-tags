package de.raik.friendtags.playermenu;

import de.raik.friendtags.AdvancedFriendTagsAddon;
import de.raik.friendtags.playermenu.entries.TagAddingEntry;
import de.raik.friendtags.playermenu.entries.TagRemovingEntry;
import net.labymod.api.events.UserMenuActionEvent;
import net.labymod.user.User;
import net.labymod.user.util.UserActionEntry;
import net.minecraft.client.network.NetworkPlayerInfo;
import net.minecraft.entity.player.EntityPlayer;

import java.util.List;

/**
 * Class with the Player Menu
 * Listener to add the entries if enabled
 *
 * @author Raik
 * @version 1.0
 */
public class PlayerMenuEditor implements UserMenuActionEvent {

    /**
     * The addon instance to access friendtags
     * and settings
     */
    private final AdvancedFriendTagsAddon addon;

    /**
     * Entry for tag adding
     */
    private final TagAddingEntry tagAddingEntry;

    /**
     * Entry for tag removing
     */
    private final TagRemovingEntry tagRemovingEntry;

    /**
     * Constructor to set addon and init
     * Player menu action executor
     * @see net.labymod.user.util.UserActionEntry.ActionExecutor
     *
     * @param addon The addon instance
     */
    public PlayerMenuEditor(AdvancedFriendTagsAddon addon) {
        this.addon = addon;
        this.tagAddingEntry = new TagAddingEntry(addon);
        this.tagRemovingEntry = new TagRemovingEntry(addon);
    }

    /**
     * Listener method to add edit the menu
     *
     * @param user The LabyMod User
     * @param entityPlayer The entity
     * @param networkPlayerInfo The networkPlayerInfo
     * @param list The player list entry list
     */
    @Override
    public void createActions(User user, EntityPlayer entityPlayer, NetworkPlayerInfo networkPlayerInfo, List<UserActionEntry> list) {
        //Cancel if disabled
        if (!this.addon.isEnabled() || !this.addon.isPlayerMenuEntriesEnabled())
            return;

        //Adding to list
        list.add(this.addon.getFriendTags().containsKey(user.getUuid()) ? this.tagRemovingEntry : this.tagAddingEntry);
    }
}
