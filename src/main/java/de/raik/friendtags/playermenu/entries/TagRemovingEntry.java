package de.raik.friendtags.playermenu.entries;

import de.raik.friendtags.AdvancedFriendTagsAddon;
import net.labymod.main.lang.LanguageManager;
import net.labymod.user.User;
import net.labymod.user.util.UserActionEntry;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiYesNo;
import net.minecraft.client.network.NetworkPlayerInfo;
import net.minecraft.entity.player.EntityPlayer;

/**
 * Entry for removing a tag with the player menu
 * @see net.labymod.user.util.UserActionEntry
 *
 * @author Raik
 * @version 1.0
 */
public class TagRemovingEntry extends UserActionEntry {

    /**
     * Constructor for init with super
     * Using own ActionExecutor
     * 
     * @param addon Addon instance needed for executor
     */
    public TagRemovingEntry(AdvancedFriendTagsAddon addon) {
        super("Remove Friend tag", EnumActionType.NONE, null, new TagRemovingActionExecutor(addon));
    }

    /**
     * Action Executor class for handling own action
     *
     * @see net.labymod.user.util.UserActionEntry.ActionExecutor
     */
    private static class TagRemovingActionExecutor implements ActionExecutor {

        /**
         * Addon instance for handling execute
         */
        private final AdvancedFriendTagsAddon addon;

        /**
         * Constructor to set
         * addon
         *
         * @param addon The addon instance
         */
        public TagRemovingActionExecutor(AdvancedFriendTagsAddon addon) {
            this.addon = addon;
        }

        /**
         * Callback method called on execute
         *
         * @param user The LabyMod User
         * @param entityPlayer The entity
         * @param networkPlayerInfo The networkPlayerInfo
         */
        @Override
        public void execute(User user, EntityPlayer entityPlayer, NetworkPlayerInfo networkPlayerInfo) {
            Minecraft.getMinecraft().displayGuiScreen(new GuiYesNo((result, id) -> {
                if (result) {
                    addon.getFriendTags().remove(user.getUuid());
                    addon.saveConfig();
                }
                Minecraft.getMinecraft().displayGuiScreen(null);
            }, LanguageManager.translate("warning_delete"), "§c" + addon.getTagEntryDisplayName(user.getUuid()), 0));
        }

        /**
         * Method which tells the menu
         * if it should show or not
         *
         * @param user The LabyMod User
         * @param entityPlayer The entity
         * @param networkPlayerInfo the networkPlayerInfo
         * @return Every time true because it will only be added if it can show
         */
        @Override
        public boolean canAppear(User user, EntityPlayer entityPlayer, NetworkPlayerInfo networkPlayerInfo) {
            return true;
        }
    }
}
