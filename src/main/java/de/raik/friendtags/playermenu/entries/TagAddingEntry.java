package de.raik.friendtags.playermenu.entries;

import de.raik.friendtags.AdvancedFriendTagsAddon;
import de.raik.friendtags.gui.FriendEditTagGui;
import net.labymod.user.User;
import net.labymod.user.util.UserActionEntry;
import net.minecraft.client.Minecraft;
import net.minecraft.client.network.NetworkPlayerInfo;
import net.minecraft.entity.player.EntityPlayer;

/**
 * Entry for adding a tag with the player menu
 * @see net.labymod.user.util.UserActionEntry
 *
 * @author Raik
 * @version 1.0
 */
public class TagAddingEntry extends UserActionEntry {

    /**
     * Constructor for init
     * with super
     * Using own ActionExecutor
     *
     * @param addon Addon instance needed for executor
     */
    public TagAddingEntry(AdvancedFriendTagsAddon addon) {
        super("Add Friend tag", EnumActionType.NONE, null, new TagAddingActionExecutor(addon));
    }

    /**
     * Action Executor class for handling
     * own action
     *
     * @see net.labymod.user.util.UserActionEntry.ActionExecutor
     */
    private static class TagAddingActionExecutor implements ActionExecutor {

        /**
         * Addon instance for handling execute
         */
        private final AdvancedFriendTagsAddon addon;

        /**
         * Constructor to set
         * addon
         *
         * @param addon The addon instance
         */
        public TagAddingActionExecutor(AdvancedFriendTagsAddon addon) {
            this.addon = addon;
        }

        /**
         * Callback method called on execute
         *
         * @param user The LabyMod User
         * @param entityPlayer The entity
         * @param networkPlayerInfo The networkPlayerInfo
         */
        @Override
        public void execute(User user, EntityPlayer entityPlayer, NetworkPlayerInfo networkPlayerInfo) {
            Minecraft.getMinecraft().displayGuiScreen(new FriendEditTagGui(user.getUuid(), this.addon, entityPlayer.getName()));
        }

        /**
         * Method which tells the menu
         * if it should show or not
         *
         * @param user The LabyMod User
         * @param entityPlayer The entity
         * @param networkPlayerInfo the networkPlayerInfo
         * @return Every time true because it will only be added if it can show
         */
        @Override
        public boolean canAppear(User user, EntityPlayer entityPlayer, NetworkPlayerInfo networkPlayerInfo) {
            return true;
        }
    }
}
