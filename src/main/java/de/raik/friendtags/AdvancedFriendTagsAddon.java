package de.raik.friendtags;

import com.google.gson.JsonObject;
import de.raik.friendtags.gui.FriendTagsGui;
import de.raik.friendtags.playermenu.PlayerMenuEditor;
import de.raik.friendtags.serverinteraction.DisablingServerMessageListener;
import de.raik.friendtags.serverinteraction.ReEnablingOnQuitListener;
import net.labymod.api.EventManager;
import net.labymod.api.LabyModAddon;
import net.labymod.gui.elements.Tabs;
import net.labymod.settings.elements.*;
import net.labymod.utils.Material;
import net.labymod.utils.UUIDFetcher;

import java.util.HashMap;
import java.util.List;
import java.util.UUID;

/**
 * Addon class
 *
 * @author Raik
 * @version 1.0
 */
public class AdvancedFriendTagsAddon extends LabyModAddon {

    /**
     * Represents the current enable status of the addon
     * Default value true
     */
    private boolean enabled = true;

    /**
     * Represents the current chat friends enable status of the addon
     * default value true
     * If enabled give a tack to labymod chat friends.
     */
    private boolean labyModChatFriends = true;

    /**
     * The format of the laby chat friend tag
     */
    private String labyChatFriendFormat = "&2&lFriend";

    /**
     * The format of the friend tag
     */
    private String friendFormat = "&lFriend";

    /**
     * The friendtags map
     */
    private HashMap<UUID, String> friendTags = new HashMap<>();

    /**
     * Value to check if the player menu is enabled
     */
    private boolean playerMenuEntries = true;

    /**
     * Represents if the server allows the addon
     */
    private boolean allowed = true;

    /**
     * Addon method to enable the
     * addon features
     */
    @Override
    public void onEnable() {
        EventManager eventManager = getApi().getEventManager();

        //Registering the render event
        eventManager.register(new NameTagRenderer(this));

        //Registering Player Menu Event
        eventManager.register(new PlayerMenuEditor(this));

        //Registering Server Interaction Listener
        eventManager.register(new DisablingServerMessageListener(this));
        eventManager.registerOnQuit(new ReEnablingOnQuitListener(this));

        //Registering Gui
        FriendTagsGui.init(this);
        Tabs.getTabUpdateListener().add(map -> map.put("Friend Tags", new Class[]{FriendTagsGui.class}));
    }

    /**
     * Addon method to load the config
     * Addon settings will be set to default
     * if no exists
     */
    @Override
    public void loadConfig() {
        JsonObject labyModAddonConfig = getConfig();

        //Enabled setting
        this.enabled = labyModAddonConfig.has("enabled") ?
                labyModAddonConfig.get("enabled").getAsBoolean() : this.enabled;

        //LabyModChat Friends settings
        this.labyModChatFriends = labyModAddonConfig.has("labychatfriends") ?
                labyModAddonConfig.get("labychatfriends").getAsBoolean() : this.labyModChatFriends;

        //Chat friend tag
        this.labyChatFriendFormat = labyModAddonConfig.has("labychatfriendformat") ?
                labyModAddonConfig.get("labychatfriendformat").getAsString() : this.labyChatFriendFormat;

        //Friend tag
        this.friendFormat = labyModAddonConfig.has("friendformat") ? labyModAddonConfig.get("friendformat")
                .getAsString() : this.friendFormat;

        //Custom set friend tags
        if (labyModAddonConfig.has("friendtags"))
            labyModAddonConfig.getAsJsonObject("friendtags").entrySet().forEach(element ->
                    friendTags.put(UUID.fromString(element.getKey()), element.getValue().getAsString()));

        //PlayerMenu Setting
        this.playerMenuEntries = labyModAddonConfig.has("playermenu") ? labyModAddonConfig.get("playermenu").
                getAsBoolean() : this.playerMenuEntries;

    }

    /**
     * Addon method to fill the settings
     *
     * @param settings The list the settings have to be added to
     */
    @Override
    protected void fillSettings(List<SettingsElement> settings) {
        //Enabled setting
        settings.add(new BooleanElement("Enabled", this, new ControlElement.IconData(Material.LEVER)
                , "enabled", this.enabled));

        //LabyModChat Friends settings
        settings.add(new BooleanElement("LabyChat Friends", this, new ControlElement.IconData(
                "labymod/textures/labymod_logo.png"), "labychatfriends", this.labyModChatFriends));

        //Player Menu display Setting
        settings.add(new BooleanElement("PlayerMenu Entries", this, new ControlElement.IconData(
                "labymod/textures/settings/settings/playermenu.png"), "playermenu", this.playerMenuEntries));

        //Head for formatting
        settings.add(new HeaderElement("Formatting"));

        //Friend tag
        settings.add(new StringElement("Tag Format", this, new ControlElement.IconData(
                "labymod/addons/advancedfriendtags/tagformat.png"), "friendformat", this.friendFormat));

        //Chat friend tag
        settings.add(new StringElement("LabyChat Tag Format", this, new ControlElement.IconData(
                "labymod/addons/advancedfriendtags/tagformatchat.png"), "labychatfriendformat", this.labyChatFriendFormat));
    }

    /**
     * Added sown saveConfig because of adding the friend tags
     */
    @Override
    public void saveConfig() {
        //Adding friend Tags
        JsonObject friendsObject = new JsonObject();
        this.friendTags.forEach((target, name) -> friendsObject.addProperty(target.toString(), name));
        this.getConfig().add("friendtags", friendsObject);

        //Saving config
        super.saveConfig();
    }

    /**
     * Get enabled value
     *
     * @return The value
     */
    public boolean isEnabled() {
        return this.enabled;
    }

    /**
     * Get labyModChatFriendsValue
     *
     * @return The value
     */
    public boolean isLabyModChatFriendsEnabled() {
        return this.labyModChatFriends;
    }

    /**
     * Get the laby chat format
     *
     * @return The value
     */
    public String getLabyChatFriendFormat() {
        return this.labyChatFriendFormat;
    }

    /**
     * Get the chat format
     *
     * @return the value
     */
    public String getFriendFormat() {
        return this.friendFormat;
    }

    /**
     * Get the friend tags Map
     *
     * @return The map
     */
    public HashMap<UUID, String> getFriendTags() {
        return friendTags;
    }

    /**
     * Get the enable status of the player menu entries
     *
     * @return The value
     */
    public boolean isPlayerMenuEntriesEnabled() {
        return this.playerMenuEntries;
    }

    /**
     * Get the allowed value
     *
     * @return The value
     */
    public boolean isAllowed() {
        return allowed;
    }

    /**
     * Set the allowed value
     * on change
     *
     * @param allowed The new value
     */
    public void setAllowed(boolean allowed) {
        this.allowed = allowed;
    }

    /**
     * Uitl method to get the display
     * name of entry
     *
     * @param uuid The entry uuid
     * @return The displayName
     */
    public String getTagEntryDisplayName(UUID uuid) {
        String displayName = UUIDFetcher.getName(uuid);

        //If no name return uuid
        if (displayName == null)
            return uuid.toString();

        return displayName;
    }
}
