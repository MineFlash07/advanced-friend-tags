package de.raik.friendtags.gui;

import de.raik.friendtags.AdvancedFriendTagsAddon;
import net.labymod.gui.elements.Scrollbar;
import net.labymod.gui.elements.Tabs;
import net.labymod.main.LabyMod;
import net.labymod.main.lang.LanguageManager;
import net.labymod.utils.DrawUtils;
import net.labymod.utils.ModColor;
import net.labymod.utils.ModUtils;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiYesNo;
import net.minecraft.client.renderer.GlStateManager;

import java.io.IOException;
import java.util.Map;
import java.util.UUID;

/**
 * Gui For Adding
 * friend tags
 *
 * Copy of GuiTags GUI {@see net.labymod.gui.GuiTags}
 *
 * @author Raik
 * @version 1.0
 */
public class FriendTagsGui extends GuiScreen {

    /**
     * Scrollbar for the tags
     */
    private Scrollbar scrollbar = new Scrollbar(29);

    /**
     * The current selected Tag
     */
    private UUID selectedTag = null;

    /**
     * The tag which is hovered
     */
    private UUID hoveredTag = null;

    /**
     * The addon instance
     */
    private static AdvancedFriendTagsAddon addon;

    /**
     * Static method to set the addon gui
     * because Tabs doesn't support variable constructor
     *
     * @param addonToSet The addon instance
     */
    public static void init(AdvancedFriendTagsAddon addonToSet) {
        addon = addonToSet;
    }

    /**
     * Setting up the gui
     */
    public void initGui() {
        super.initGui();

        //Setting Scrollbar
        this.scrollbar.init();
        this.scrollbar.setPosition(this.width / 2 + 102, 44, this.width / 2 + 106, this.height - 32 - 3);
        this.scrollbar.setSpeed(10);

        //Init Button
        this.buttonList.add(new GuiButton(0, this.width / 2 - 120, this.height - 26, 75, 20, LanguageManager.translate("button_remove")));
        this.buttonList.add(new GuiButton(1, this.width / 2 - 37, this.height - 26, 75, 20, LanguageManager.translate("button_edit")));
        this.buttonList.add(new GuiButton(2, this.width / 2 + 120 - 75, this.height - 26, 75, 20, LanguageManager.translate("button_add")));

        //Init Tabs
        Tabs.initGuiScreen(this.buttonList, this);
    }

    protected void actionPerformed(GuiButton button) throws IOException {
        super.actionPerformed(button);

        switch (button.id) {
            case 0:
                Minecraft.getMinecraft().displayGuiScreen(new GuiYesNo((result, id) -> {
                    if (result) {
                        addon.getFriendTags().remove(this.selectedTag);
                        addon.saveConfig();
                    }
                    Minecraft.getMinecraft().displayGuiScreen(this);
                }, LanguageManager.translate("warning_delete"), "§c" + addon.getTagEntryDisplayName(this.selectedTag), 0));
                break;
            case 1:
                Minecraft.getMinecraft().displayGuiScreen(new FriendEditTagGui(this, this.selectedTag, addon));
                break;
            case 2:
                Minecraft.getMinecraft().displayGuiScreen(new FriendEditTagGui(this, null, addon));
                break;

        }

        Tabs.actionPerformedButton(button);
    }

    /**
     * Drawing the gui
     *
     * @param mouseX mouseX
     * @param mouseY mouseY
     * @param partialTicks partialTicks
     */
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
        DrawUtils labyModDrawUtils = LabyMod.getInstance().getDrawUtils();

        //Drawing background
        labyModDrawUtils.drawAutoDimmedBackground(this.scrollbar.getScrollY());

        //Reset hoveredTag
        this.hoveredTag = null;

        //Drawing Entries
        double yPosition = 45.0D + this.scrollbar.getScrollY() + 3.0D;

        for (Map.Entry<UUID, String> mapEntry: addon.getFriendTags().entrySet()) {
            //Draw
            this.drawEntry(addon.getTagEntryDisplayName(mapEntry.getKey()), mapEntry.getValue(), yPosition, mouseX, mouseY, mapEntry.getKey(), labyModDrawUtils);

            //Increase yPosition
            yPosition += 29.0D;
        }

        //Drawing other things
        labyModDrawUtils.drawOverlayBackground(0, 41);
        labyModDrawUtils.drawOverlayBackground(this.height - 32, this.height);
        labyModDrawUtils.drawGradientShadowTop(41.0D, 0.0D, this.width);
        labyModDrawUtils.drawGradientShadowBottom(this.height - 32, 0.0D, this.width);
        labyModDrawUtils.drawCenteredString("Friend Tags", (double) (this.width / 2), 29.0D);

        //Editing gui components
        this.scrollbar.update(addon.getFriendTags().size());
        this.scrollbar.draw();
        this.buttonList.get(0).enabled = this.selectedTag != null;
        this.buttonList.get(1).enabled = this.selectedTag != null;

        super.drawScreen(mouseX, mouseY, partialTicks);
    }

    /**
     * Drawing tag entry
     *
     * @param name The name of the player
     * @param tag The tag of the player
     * @param y Y
     * @param mouseX mouseX
     * @param mouseY mouseY
     * @param currentKey The uuid key of the user
     * @param drawUtils LabyMod Drawing Utils
     */
    private void drawEntry(String name, String tag, double y, int mouseX, int mouseY, UUID currentKey, DrawUtils drawUtils) {
        int x = this.width / 2 - 100;

        int borderColor = this.selectedTag == currentKey ? ModColor.toRGB(240, 240, 240, 240) : -2147483648;
        int backgroundColor = ModColor.toRGB(30, 30, 30, 120);

        //Setting hovered entry
        if (mouseX > x && mouseX < x + 200 && (double) mouseY > y && (double) mouseY < y + 24.0D &&  mouseX > 32 && mouseY < this.height - 32) {
            this.hoveredTag = currentKey;
            //Change background color on hover
            backgroundColor = ModColor.toRGB(50, 50, 50, 120);
        }

        //Drawing box
        drawRect(x - 5, (int) y - 4, x + 200, (int) y + 24, backgroundColor);
        drawUtils.drawRectBorder(x - 5, (int) y - 4, (double) (x + 200), (double) ((int) y + 24), borderColor, 1.0D);
        GlStateManager.color(1.0F, 1.0F, 1.0F, 1.0F);
        drawUtils.drawPlayerHead(currentKey, x, (int) y, 20);
        drawUtils.drawString(name, x + 25, y + 1.0D);
        drawUtils.drawString(drawUtils.trimStringToWidth(ModUtils.translateAlternateColorCodes('&', tag)
                , 200 - 20 - 10), x + 25, y + 11.0D);
    }

    /**
     * Handling Mouse clicking
     *
     * @param mouseX mouseX
     * @param mouseY mouseY
     * @param mouseButton mouseButton
     * @throws IOException Throwing exception on error
     */
    protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException {
        super.mouseClicked(mouseX, mouseY, mouseButton);

        //Setting selected tag
        if (this.hoveredTag != null)
            this.selectedTag = this.hoveredTag;

        //Handling Scrollbar
        this.scrollbar.mouseAction(mouseX, mouseY, Scrollbar.EnumMouseAction.CLICKED);
    }

    /**
     * Handling mouse click moving
     *
     * @param mouseX mouseX
     * @param mouseY mouseY
     * @param clickedMouseButton clickedMouseButton
     * @param timeSinceLastClick timeSinceLastClick
     */
    protected void mouseClickMove(int mouseX, int mouseY, int clickedMouseButton, long timeSinceLastClick) {
        super.mouseClickMove(mouseX, mouseY, clickedMouseButton, timeSinceLastClick);

        //Handling Scrollbar
        this.scrollbar.mouseAction(mouseX, mouseY, Scrollbar.EnumMouseAction.DRAGGING);
    }

    /**
     * Handling mouse release
     *
     * @param mouseX mouseX
     * @param mouseY mouseY
     * @param state state
     */
    protected void mouseReleased(int mouseX, int mouseY, int state) {
        //Handling scrollbar
        this.scrollbar.mouseAction(mouseX, mouseY, Scrollbar.EnumMouseAction.RELEASED);

        super.mouseReleased(mouseX, mouseY, state);
    }

    /**
     * Handling mouse input
     *
     * @throws IOException Throwing exception on error
     */
    public void handleMouseInput() throws IOException {
        super.handleMouseInput();

        //Handling scrollbar
        this.scrollbar.mouseInput();
    }
}
