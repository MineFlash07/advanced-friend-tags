package de.raik.friendtags.gui;

import de.raik.friendtags.AdvancedFriendTagsAddon;
import net.labymod.gui.elements.ModTextField;
import net.labymod.main.LabyMod;
import net.labymod.main.lang.LanguageManager;
import net.labymod.utils.DrawUtils;
import net.labymod.utils.ModColor;
import net.labymod.utils.ModUtils;
import net.labymod.utils.UUIDFetcher;
import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.gui.GuiYesNo;
import org.lwjgl.input.Keyboard;

import java.awt.*;
import java.io.IOException;
import java.util.UUID;

/**
 * Gui for editing
 * a specific friend tag
 *
 * Copy of GuiTags add {@see net.labymod.gui.GuiTagsAdd}
 *
 * @author Raik
 * @version 1.0
 */
public class FriendEditTagGui extends GuiScreen {

    /**
     * The screen that was displayed before
     * saved for getting back
     */
    private final GuiScreen screenBefore;

    /**
     * The text field where you have to put
     * in the name or uuid of your target
     */
    private ModTextField targetKeyField;

    /**
     * The field where the tag
     * of the target is displayed in.
     */
    private ModTextField tagField;

    /**
     * Value if the tag is added or edited
     */
    private final boolean isEditing;

    /**
     * The target before to check if it has changed
     */
    private UUID targetBefore = null;

    /**
     * The target before as string for performant comparing
     */
    private String targetBeforeString = null;

    /**
     * The tag before to check if it has changed
     */
    private String tagBefore = null;

    /**
     * Addon instance for handling tags
     */
    private final AdvancedFriendTagsAddon addon;

    /**
     * Saving uuid of tag to render
     * because of performance and rate limits
     */
    private UUID renderHead = null;

    /**
     * Value if userName is not found
     */
    private boolean userNameNotFound = false;

    /**
     * The start time of the animation
     */
    private long errorAnimationStart = 0L;

    /**
     * The position modifier for the animation
     */
    private int animationModifier = 0;

    /**
     * The last typed key to save it for yes no question
     */
    private String typedKey = "";

    /**
     * The last typed tag to save it for yes no question
     */
    private String typedTag;

    /**
     * The constructor to set the attributes
     *
     * @param screenBefore The screen which was displayed before
     * @param target The target uuid
     * @param addon The addon instance for handling the texts
     */
    public FriendEditTagGui(GuiScreen screenBefore, UUID target, AdvancedFriendTagsAddon addon) {
        this.screenBefore = screenBefore;
        this.isEditing = target != null;
        this.addon = addon;

        //Setting the things before and edit
        if (target == null) {
            this.typedTag = addon.getFriendFormat();
            return;
        }

        this.targetBefore = target;
        this.targetBeforeString = addon.getTagEntryDisplayName(target);
        this.renderHead = target;
        this.tagBefore = addon.getFriendTags().get(target);
        this.typedKey = this.targetBeforeString;
        this.typedTag = this.tagBefore;
    }

    /**
     * Constructor to init adding
     * with player menu
     *
     * @param target The target UUID
     * @param addon The addon instance for handling texts
     * @param keyField The name of the player selected in the Addon menu
     */
    public FriendEditTagGui(UUID target, AdvancedFriendTagsAddon addon, String keyField) {
        /*
         * Executing main constructor for setup things
         * Screen before is null because it its opened with
         * player menu
         */
        this(null, target, addon);
        //Setting the name of the player the tag is for
        this.typedKey = keyField;
        this.typedTag = addon.getFriendFormat();
    }

    /**
     * Method to setup the gui components
     */
    public void initGui() {
        super.initGui();
        Keyboard.enableRepeatEvents(true);

        //Set text fields
        int x = this.width / 2 - 100;
        this.targetKeyField = new ModTextField(-1, LabyMod.getInstance().getDrawUtils().fontRenderer, x, this.height / 2 - 50, 200, 20);
        //Setting max length to uuid length
        this.targetKeyField.setMaxStringLength(36);

        this.tagField = new ModTextField(-2, LabyMod.getInstance().getDrawUtils().fontRenderer, x, this.height / 2 - 5, 200, 20);
        this.tagField.setColorBarEnabled(true);

        //Setting text
        this.targetKeyField.setText(typedKey);
        this.tagField.setText(this.typedTag);

        //Setting buttons
        this.buttonList.add(new GuiButton(0, this.width / 2 + 3, this.height / 2 + 35, 98,
                20, LanguageManager.translate(this.isEditing ? "button_save" : "button_add")));
        this.buttonList.add(new GuiButton(1, this.width / 2 - 101, this.height / 2 + 35, 98,
                20, LanguageManager.translate("button_cancel")));
        this.changeButtonStatus();
    }

    protected void actionPerformed(GuiButton button) throws IOException {
        super.actionPerformed(button);
        switch (button.id) {
            case 0:
                UUID target = this.resolveUUIDFromKeyField();

                if (target == null) {
                    this.userNameNotFound = true;
                    this.errorAnimationStart = System.currentTimeMillis();
                    this.animationModifier = 1;
                    break;
                }

                //Using adding things
                if (!this.isEditing) {
                    //Handling already contained entry
                    if (this.addon.getFriendTags().containsKey(target)) {
                        //Setting type value to save them
                        this.typedKey = this.targetKeyField.getText();
                        this.typedTag = this.tagField.getText();

                        Minecraft.getMinecraft().displayGuiScreen(new GuiYesNo((result, id) -> {
                            if (result) {
                                this.addon.getFriendTags().replace(target, this.tagField.getText());
                                this.addon.saveConfig();
                                Minecraft.getMinecraft().displayGuiScreen(this.screenBefore);
                            } else
                                Minecraft.getMinecraft().displayGuiScreen(this);
                        },"§cThe user §e" + this.addon.getTagEntryDisplayName(target) + " §chas already a tag. Do you want to overwrite? The current tag:",
                                ModUtils.translateAlternateColorCodes('&', this.addon.getFriendTags().get(target)), 0));
                        break;
                    }
                    //Adding entry
                    this.addon.getFriendTags().put(target, this.tagField.getText());
                    this.addon.saveConfig();
                    Minecraft.getMinecraft().displayGuiScreen(this.screenBefore);
                    break;
                }
                //Edit entry
                this.addon.getFriendTags().remove(this.targetBefore);
                this.addon.getFriendTags().put(target, this.tagField.getText());
                this.addon.saveConfig();
                Minecraft.getMinecraft().displayGuiScreen(this.screenBefore);
                break;
            case 1:
                //Cancel button
                Minecraft.getMinecraft().displayGuiScreen(this.screenBefore);
                break;
        }
    }

    /**
     * Drawing gui components
     *
     * @param mouseX mouseX
     * @param mouseY mouseY
     * @param partialTicks partialTicks
     */
    public void drawScreen(int mouseX, int mouseY, float partialTicks) {
        //Drawing components
        this.drawDefaultBackground();
        this.targetKeyField.drawTextBox();
        this.tagField.drawTextBox();
        this.tagField.drawColorBar(mouseX, mouseY);

        DrawUtils labyModDrawUtils = LabyMod.getInstance().getDrawUtils();

        //Drawing head
        if (this.renderHead != null) {
            labyModDrawUtils.drawPlayerHead(this.renderHead, this.targetKeyField.xPosition - this.targetKeyField.height - 4
                    , this.targetKeyField.yPosition, this.targetKeyField.height);
        }

        //Drawing strings above text Fields
        labyModDrawUtils.drawString(LanguageManager.translate("minecraft_name"), (double) this.width / 2 - 100,
                (double)this.height / 2 - 65);
        labyModDrawUtils.drawString("Friend Tag", (double) this.width / 2 - 100, (double) this.height / 2 -20);

        //Displaying string
        String displayName = ModUtils.translateAlternateColorCodes('&', this.tagField.getText());
        int stringLength = labyModDrawUtils.getStringWidth(ModColor.removeColor(displayName));
        if (stringLength > 1) {
            drawRect(this.width / 2 - stringLength / 2 - 2, this.height / 2 - 92, this.width / 2 +
                    stringLength / 2 + 2, this.height / 2 - 80, -2147483648);
            labyModDrawUtils.drawCenteredString(displayName, (double) this.width / 2, (double) this.height / 2 - 90);
        }

        super.drawScreen(mouseX, mouseY, partialTicks);

        //Drawing error bar on error
        if (!this.userNameNotFound)
            return;

        drawRect(0, 10, this.width, 30, Color.RED.getRGB());
        labyModDrawUtils.drawCenteredString("User not found.", (double) this.width / 2 + this.animationModifier, 16.0D);

        //Animation
        if (this.errorAnimationStart + 1000L > System.currentTimeMillis()) {
            this.animationModifier *= -1;
            return;
        }

        this.errorAnimationStart = 0L;
        this.animationModifier = 0;
    }

    /**
     * Method to handle gui close
     */
    public void onGuiClosed() {
        Keyboard.enableRepeatEvents(false);
    }

    /**
     * Set button status
     * depending on editing or not
     * and if something has changed
     */
    private void changeButtonStatus() {
        String targetKeyFieldText = this.targetKeyField.getText();
        String tagFieldText = this.tagField.getText();
        this.buttonList.get(0).enabled = !targetKeyFieldText.isEmpty() && !tagFieldText.isEmpty() &&
                (!this.isEditing || !(targetKeyFieldText.equals(this.targetBeforeString) && tagFieldText.equals(this.tagBefore)));
    }

    /**
     * Handling keytyped
     *
     * @param typedChar The character typed
     * @param keyCode The keycode of the pressed button
     * @throws IOException Throwing Exception on error
     */
    protected void keyTyped(char typedChar, int keyCode) throws IOException {
        //Setting error to false on key typed
        if (this.targetKeyField.textboxKeyTyped(typedChar, keyCode))
            this.userNameNotFound = false;

        this.tagField.textboxKeyTyped(typedChar, keyCode);

        this.changeButtonStatus();

        super.keyTyped(typedChar, keyCode);

        //Changing focused field on Tab
        if (keyCode != 15)
            return;

        if (this.targetKeyField.isFocused()) {
            this.tagField.setFocused(true);
            this.targetKeyField.setFocused(false);
            this.renderHead = this.resolveUUIDFromKeyField();
            return;
        }

        if (!this.tagField.isFocused())
            return;

        this.targetKeyField.setFocused(true);
        this.tagField.setFocused(false);
    }

    /**
     * Handle mouse clicking
     *
     * @param mouseX mouseX
     * @param mouseY mouseY
     * @param mouseButton The pressed mouse button
     * @throws IOException Throwing exception on error
     */
    protected void mouseClicked(int mouseX, int mouseY, int mouseButton) throws IOException {
        //Saving focused state to decide changing renderHead
        boolean wasKeyFieldFocused = this.targetKeyField.isFocused();

        this.targetKeyField.mouseClicked(mouseX, mouseY, mouseButton);
        super.mouseClicked(mouseX, mouseY, mouseButton);

        //Saving renderHead if was focused
        if (!this.tagField.mouseClicked(mouseX, mouseY, mouseButton))
            return;

        if (!this.targetKeyField.isFocused() && wasKeyFieldFocused)
            this.renderHead = this.resolveUUIDFromKeyField();
    }

    /**
     * Handling screen updating
     */
    public void updateScreen() {
        super.updateScreen();
        this.targetKeyField.updateCursorCounter();
        this.tagField.updateCursorCounter();
    }

    /**
     * Returns the uuid of the target
     * in the text field. Supports Usernames and uuids
     *
     * @return The uuid
     */
    private UUID resolveUUIDFromKeyField() {
        try {
            //Trying to parse to UUID
            return UUID.fromString(this.targetKeyField.getText());
        } catch (IllegalArgumentException exception) {
            //Resolving UUID from Username if not uuid
            return UUIDFetcher.getUUID(this.targetKeyField.getText());
        }
    }

}
